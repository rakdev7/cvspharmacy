
import UIKit

class DetailViewModel{
    let image: UIImage
    let title: String
    let width: String
    let height: String
    let publishedDate: String
    
    init(image: UIImage,
         title: String,
         width: Int,
         height: Int,
         publishedDate: String) {
        self.image = image
        self.title = title
        self.width = String(width)
        self.height = String(height)
        self.publishedDate = publishedDate
    }
}
