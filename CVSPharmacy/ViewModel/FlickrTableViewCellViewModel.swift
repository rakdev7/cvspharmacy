
import Foundation

class FlickrTableViewCellViewModel{
    let title       : String
    let imageURL    : String
    let width       : Int
    let height      : Int
    let published   : String
    
    init(title: String,
         imageUrl: String,
         width: Int,
         height: Int,
         published: String) {
        self.title     = title
        self.imageURL  = imageUrl
        self.width     = width
        self.height    = height
        self.published = published
    }
}
