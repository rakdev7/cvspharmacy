import Foundation

final class APICaller {
    static let shared = APICaller()
    
    struct Constants {
        static let flicKrAPI = "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1&tags="
    }
    
    public func getResults(query: String, completion: @escaping (Result<[Items],Error>) -> Void) {
        let queryString = Constants.flicKrAPI + query
        guard let url = URL(string: queryString) else {
                return
            }
            let task = URLSession.shared.dataTask(with: url) { data, _, error in
                if let error = error {
                    completion(.failure(error))
                }
                else if let data = data {
                    do {
                        let result = try JSONDecoder().decode(APIResponse.self, from: data)
                        completion(.success(result.items))
                    }
                    catch {
                        completion(.failure(error))
                    }
                }
            }
            task.resume()
        }
    }
