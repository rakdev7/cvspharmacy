
import UIKit

class FlickrTableViewCell: UITableViewCell {
    static let identifier = "FlickrTableViewCell"
    
    @IBOutlet weak var flickrImageView: UIImageView!
        
    func configure(with viewModel: FlickrTableViewCellViewModel) {
        guard let url = URL(string: viewModel.imageURL) else { return }
        URLSession.shared.dataTask(with: url) { [weak self] data, _, error in
                guard let data = data,error == nil else{
                    return
                }
                DispatchQueue.main.async {
                    self?.flickrImageView.image = UIImage(data: data)
                }
            }.resume()
        
    }
}
