import Foundation

struct APIResponse:Codable {
   let items: [Items]
}
struct Items: Codable {
    let title       : String
    let description : String
    let media       : Media
    let published   : String
}
struct Media: Codable{
    let imageUrl: String
    
    enum CodingKeys: String,CodingKey{
        case imageUrl = "m"
    }
}
