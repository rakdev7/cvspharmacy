
import UIKit

class DetailedViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var imageWidth: UILabel!
    
    @IBOutlet weak var imageHeight: UILabel!
    
    @IBOutlet weak var publishedLabel: UILabel!
    
    var detailViewModel: DetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imageView.image   = detailViewModel?.image
        titleLabel.text   = detailViewModel?.title
        imageWidth.text   = detailViewModel?.width
        imageHeight.text  = detailViewModel?.height
        publishedLabel.text = detailViewModel?.publishedDate
    }
}
