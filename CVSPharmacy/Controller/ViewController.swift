
import UIKit

class ViewController: UIViewController {
    private let searchVC = UISearchController(searchResultsController: nil)
    private var viewModels = [FlickrTableViewCellViewModel]()
    private var items = [Items]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Flickr"
        createSearchBar()
    }
  
    private func createSearchBar() {
        navigationItem.searchController = searchVC
        searchVC.searchBar.delegate = self
    }
    
    private func fetchItems(queryString : String) {
        APICaller.shared.getResults(query: queryString) {[weak self] result in
            switch result {
            case .success(let items):
                self?.viewModels = items.compactMap({ item in
                let tuple = FlickrUtilities.extractWidthAndHeight(description:item.description)
                    return FlickrTableViewCellViewModel(title: item.title.trimmingCharacters(in: .whitespaces).isEmpty ? "No title" : item.title,
                                                        imageUrl: item.media.imageUrl,
                                                        width: tuple.width ?? 0,
                                                        height: tuple.height ?? 0,
                                                        published: item.published)
                })
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
                break
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       guard let cell = tableView.dequeueReusableCell(
            withIdentifier: FlickrTableViewCell.identifier,
            for: indexPath) as? FlickrTableViewCell else {
           return UITableViewCell()
       }
        cell.configure(with: viewModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let vc = storyboard?.instantiateViewController(withIdentifier: "DetailedViewController") as? DetailedViewController {
            let flickerViewModel = viewModels[indexPath.row]
            guard let cell = tableView.cellForRow(at: indexPath) as? FlickrTableViewCell else {
                return
            }
            vc.detailViewModel = DetailViewModel(image: cell.flickrImageView.image ?? UIImage() ,
                title: flickerViewModel.title,
                                                 width: flickerViewModel.width,
                                                 height: flickerViewModel.height,
                                                 publishedDate: flickerViewModel.published)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Float(viewModels[indexPath.row].height))
    }
}

extension ViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        fetchItems(queryString: searchText)
    }
}
