import Foundation

struct FlickrUtilities {
    static func extractWidthAndHeight(description: String) -> (width: Int?, height: Int?) {
        let inputString = description
        var width = String()
        var height = String()
        if let widthRange = inputString.range(of: "width=\"") {
            if let endOfWidthRange = inputString.range(of: "\"",
                                                       range: widthRange.upperBound..<inputString.endIndex) {
                width = String(inputString[widthRange.upperBound..<endOfWidthRange.lowerBound])
       
            }
        }
        if let heightRange = inputString.range(of: "height=\"") {
            if let endOfHeightRange = inputString.range(of: "\"",
                                                        range: heightRange.upperBound..<inputString.endIndex) {
                height = String(inputString[heightRange.upperBound..<endOfHeightRange.lowerBound])
            }
        }
        return (Int(width), Int(height))
    }
}
